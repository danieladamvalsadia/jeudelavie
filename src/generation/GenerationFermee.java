package generation;

/**
 * La classe GenerationFermee represente une generation fermee.
 * Une generation fermee est une representation du jeu de la vie avec une limite de taille.
 *
 * @author Groupe 3
 * @see Generation
 */
public class GenerationFermee extends Generation {

    /**
     * La methode getInstance retourne une instance de generation fermee.
     *
     * @return GenerationFermee retourne une nouvelle generation fermee.
     */
    @Override
    Generation getInstance() {
        return new GenerationFermee();
    }

    /**
     * La methode estValide verifie si une cellule est valide. En effet, une generation fermee impose à une cellule
     * d'être dans la zone de jeu. Si une cellule est dans la zone, la methode retourne vrai sinon faux.
     *
     * @param c est une Cellule dont on veut savoir si elle est valide.
     * @return Boolean {@code true} si la cellule est dans la generation, faux sinon.
     */
    @Override
    boolean estValide(Cellule c) {
        int x = c.getX(), y = c.getY();
        return (min_x <= x) && (x <= max_x) && (min_y <= y) && (y <= max_y);
    }


    /**
     * La methode getCellule retourne une cellule en parametre.
     *
     * @param c est une cellule dont on veut obtenir la version correcte pour la generation.
     * @return Cellule c est une cellule correcte.
     */
    @Override
    Cellule getCellule(Cellule c) {
        return c;
    }
}