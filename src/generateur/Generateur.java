package generateur;

import gestionJeu.JeuGeneration;

/**
 *  L'inteface Generateur sert de générateur
 *  pour créer de nouvelle instance d'un objet
 *  de type JeuGeneration.
 *
 *  @see JeuGeneration
 *  @author Groupe 3
 */
@FunctionalInterface
public interface Generateur {

    /**
     *  Méthode permettant de générer une
     *  nouvelle instance de JeuGeneration.
     *
     *  @return JeuGeneration Une nouvelle instance d'un objet de type JeuGeneration
     */
    JeuGeneration generer();
}
